#!/usr/bin/env bash

# Get LDAP_* info and 'DAVMAIL_CODE'
. ~/.appauth

# curl -u "${LDAP_NAME}@wnco.com:${LDAP_PASSWORD}" -vvv http://${DAVMAIL_SERVER}:${DAVMAIL_HTTP_PORT}/users/${LDAP_NAME}@wnco.com/calendar/

#curl -vvv --cookie-jar cookies.txt --cookie cookies.txt \
#	--user "${LDAP_NAME}@wnco.com:${LDAP_PASSWORD}" \
#	"imap://${DAVMAIL_SERVER}:${DAVMAIL_IMAP_PORT}/"

read -d'' query_xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<C:calendar-query xmlns:C="urn:ietf:params:xml:ns:caldav" xmlns:D="DAV:">
	<D:prop>
		<C:calendar-data />
	</D:prop>
	<C:filter>
		<C:comp-filter name="VCALENDAR">
			<C:comp-filter name="VEVENT">
				<C:time-range start="20250119T000000Z" end="20250121T000000Z"/>
			</C:comp-filter>
		</C:comp-filter>
	</C:filter>
</C:calendar-query>
EOF

# ref also: https://stackoverflow.com/questions/37711699/expanding-recurring-events-in-caldav

query_xml="${query_xml//
/ }"

# NOTE:	Unfortunately, ATM, calendar query doesn't work and returns all events. :(
#			Added 'icaltool' to filter the xml wrapped ICS output.

curl --output "${PWD}/Calendar.xml" --request 'REPORT' \
	--data "${query_xml}" \
	--header "Content-Type: text/xml; charset=utf-8" \
	--user "${LDAP_NAME}@wnco.com:${LDAP_PASSWORD}" \
	"http://${DAVMAIL_SERVER}:${DAVMAIL_HTTP_PORT}/users/${LDAP_NAME}@wnco.com/calendar/" || exit ${?}

readonly START_DT=$(date --date='-7 days' +'%Y-%m-%d')
readonly END_DT=$(date --date='+7 days' +'%Y-%m-%d')
exec ~/bin/icaltool.bash "${PWD}/Calendar.xml" --filter "COMPONENT:+VEVENT;DTSTART:+${START_DT}to${END_DT}" --output '-'
exit ${?}
