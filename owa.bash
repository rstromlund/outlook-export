#!/bin/bash

typeset pl=$(cat <<'EOF'
use Date::Manip;
use Data::Dumper qw(Dumper);
use strict;

undef $/;

sub doId($$) {
	my ($evnt, $strEvent) = @_;
	if ($strEvent =~ s!^id\(([^()]+)\)\s+!!) {
		$evnt->{'id'} = $1;
	}
	return $strEvent;
}

sub doStatus($$) {
	my ($evnt, $strEvent) = @_;
	if ($strEvent =~ s![, ]*(Busy|Tentative|OOF|Free|WorkingElsewhere)!!) {
		$evnt->{'status'} = $1;
	}
	return $strEvent;
}

sub doEvent($$) {
	my ($evnt, $strEvent) = @_;
	if ($strEvent =~ s!^event\s+!!) {
		$evnt->{'all-day'} = 0;
	}
	return $strEvent;
}

sub doAllDay($$) {
	my ($evnt, $strEvent) = @_;
	if ($strEvent =~ s![, ]*all day event!!) {
		$evnt->{'all-day'} = 1;
	}
	return $strEvent;
}

sub doStartEnd($$) {
	# E.g.: 9:05 AM to 9:20 AM, Monday, July 22, 2024
	my ($evnt, $strEvent) = @_;
	if ($strEvent =~ s![, ]*(\d+:\d+\s*.M) to (\d+:\d+\s*.M)[, ]*(\w+, \w+ \d+, \d{4})!!) {
		my ($strHrStart, $strHrEnd, $strDt) = ($1, $2, $3);
		$evnt->{'start'} = Date_ConvTZ(ParseDate("$strDt $strHrStart"), "America/Chicago", "GMT");
		$evnt->{'end'}   = Date_ConvTZ(ParseDate("$strDt $strHrEnd"), "America/Chicago", "GMT");
	}
	return $strEvent;
}

sub doStart($$) {
	my ($evnt, $strEvent) = @_;
	if (!defined $evnt->{'start'} && $strEvent =~ s![, ]*(?:from )?\w+, (\w+ \d+, \d{4}(?: \d+:\d+ .M)?)!!) {
		$evnt->{'start'} = Date_ConvTZ(ParseDate($1), "America/Chicago", "GMT");
	}
	return $strEvent;
}

sub doEnd($$) {
	my ($evnt, $strEvent) = @_;
	if (!defined $evnt->{'end'} && $strEvent =~ s![, ]*to (\w+, \w+ \d+, \d{4})?(\s*\d+:\d+ .M)?!!) {
		my $strEnd = (! defined $1)
			? substr($evnt->{'start'}, 0, 8) . substr(ParseDate($2), 8)
			: "$1 $2"
		;
		$evnt->{'end'} = Date_ConvTZ(ParseDate($strEnd), "America/Chicago", "GMT");
	}
	return $strEvent;
}

sub doFor($$) {
	my ($evnt, $strEvent) = @_;
	if ($strEvent =~ s![, ]for \w+, (\w+ \d+, \d{4}(?: \d+:\d+ .M)?)!!) {
		$evnt->{'end'} = $evnt->{'start'} = Date_ConvTZ(ParseDate($1), "America/Chicago", "GMT");
	}
	return $strEvent;
}

sub doRecur($$) {
	my ($evnt, $strEvent) = @_;
	$strEvent =~ s![, ]*, [Rr]ecurring(?: event)?!!;
	$evnt->{'recur'} = 1;
	return $strEvent;
}

sub doFrom($$) {
	my ($evnt, $strEvent) = @_;
	if ($strEvent =~ s!\s+organizer (.*)$!!) {
		$evnt->{'from'} = $1;
	} elsif ($strEvent =~ s![, ]*By ([^,]+)!!) {
		$evnt->{'from'} = $1;
	} else {
		$evnt->{'from'} = $ENV{'LOGNAME'};
	}

	if ($evnt->{'from'} =~ s!\s+(?:recurring )?event shown as (\w+)!!) {
		$evnt->{'status'} = $1;
	}
	return $strEvent;
}

sub doLoc($$) {
	my ($evnt, $strEvent) = @_;
	$strEvent =~ s!\s+location (.*)$!!;
	$evnt->{'loc'} = $1;
	$strEvent =~ s![, ]*Microsoft Teams Meeting!!;
	$evnt->{'loc'} .= 'teams';
	return $strEvent;
}

sub doSubj($$) {
	my ($evnt, $strEvent) = @_;
	$strEvent =~ s! _\w{1,4}\$$!!;
	$evnt->{'subj'} = $strEvent;
	return '';
}

sub calcSkipAndReminders($$) {
	my ($evnt, $strEvent) = @_;
	# W/o opening each event, we don't know certain settings like reminder and category.
	$evnt->{'skip'} = ($evnt->{'subj'} =~ m!(?:Import calendar|PWA|PTO|OOO|TOWOP|Dr. Appt|Dr Appt|Toastmasters|Office Hours|CAB |noimport|No appts plz)!i) || 0; ## Aka. Category == NoSpiralClock
	$evnt->{'alarm'} = (1 == $evnt->{'all-day'}) ? 'PT480M' : 'PT15M'; ## 8 hours for an all-day reminder (noon-ish); else default to 15 minutes
	return $strEvent;
}

my @aEvents = map {
	# print "DEBUG: $_\n";

	my %evnt = ('status' => 'Busy');

	calcSkipAndReminders(\%evnt,
	doSubj(\%evnt,
	doLoc(\%evnt,
	doFrom(\%evnt,
	doRecur(\%evnt,
	doFor(\%evnt,
	doEnd(\%evnt,
	doStart(\%evnt,
	doStartEnd(\%evnt,
	doAllDay(\%evnt,
	doEvent(\%evnt,
	doStatus(\%evnt,
	doId(\%evnt,
		$_)))))))))))));

	\%evnt;
} (split /\s+~\s+/, <>);

print "BEGIN:VCALENDAR
PRODID:-//localhost//Calendar.xsl
VERSION:2.0
METHOD:PUBLISH
BEGIN:VTIMEZONE
TZID:Central Standard Time
BEGIN:STANDARD
DTSTART:16011104T020000
RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:16010311T020000
RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
END:DAYLIGHT
END:VTIMEZONE\n";

sub printReminder($) {
	my ($evnt) = @_;
	return (! defined $evnt->{'alarm'}) ? '' : "BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:Reminder
TRIGGER:-$evnt->{'alarm'}
END:VALARM\n";

}

sub printEvent($) {
	my ($evnt) = @_;

	(my $from = $evnt->{'from'}) =~ s!\s+!.!g;
	my $rem = printReminder($evnt);
	my $loc = (defined $evnt->{'loc'}) ? "LOCATION:$evnt->{'loc'}\n" : '';
	my $status = $evnt->{'status'};

	(my $start = $evnt->{'start'}) =~ s!:!!g;
	$start =~ s!^(\d\d\d\d\d\d\d\d)!\1T!;
	(my $end = $evnt->{'end'}) =~ s!:!!g;
	$end =~ s!^(\d\d\d\d\d\d\d\d)!\1T!;

	if (1 == $evnt->{'all-day'}) {
		$start = ";VALUE=DATE:" . substr($start, 0, 8);
		$end = '';
		$status = 'Free';
	} else {
		$start = ":${start}Z";
		$end = "\nDTEND:${end}Z";
	}

	print "\nBEGIN:VEVENT
CLASS:PUBLIC
UID:$evnt->{'id'}
DTSTART$start$end
SUMMARY:$evnt->{'subj'}
${loc}ORGANIZER:mailto:$from\@wnco.com
STATUS:$status
${rem}END:VEVENT\n";

}

foreach my $evnt (sort {$a->{'start'} cmp $b->{'start'}} @aEvents) {
	if (0 == $evnt->{'skip'}) {
		printEvent($evnt);
	}
}

print "\nEND:VCALENDAR\n";
EOF
)

java net.sf.saxon.Transform -x org.ccil.cowan.tagsoup.Parser "${PWD}/Calendar-OWA.txt" "${PWD}/Calendar-OWA.xsl" | perl -e "${pl}"

exit ${?}
