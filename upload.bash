#!/bin/bash
cd "${0%/*}"

typeset ICS_WEBDAV_ID='<ID read from ~/.appauth>'
typeset ICS_WEBDAV_PASSWORD='<PASSWORD read from ~/.appauth>'
typeset ICS_WEBDAV_DEST='<Read from ~/.appauth; e.g. https://your.server/webdav/>'
source ~/.appauth
#set -x

typeset OLM_FILE="${HOME}/Downloads/Outlook for Mac Archive.olm"


exec 2>${0}.log
{
type java
java -version
echo PATH=$PATH
echo CLASSPATH=$CLASSPATH
} 1>&2

function transform {
	if [[ -r 'Calendar.xml' ]] ; then
		./davmail-export.bash

	elif [[ -r 'Calendar.csv' ]] ; then
		cp 'Calendar.csv' "Calendar-$(date +'%Y%m%d').csv" && \
		time java net.sf.saxon.Transform -it:main "${PWD}/Calendar-ICS.xsl" "base-dir=${PWD}"

	elif [[ -r "${OLM_FILE}" ]] ; then
		if [[ "${OLM_FILE}" -nt 'Calendar.xml' ]] ; then
			unzip -p "${OLM_FILE}" 'Accounts/*/Calendar/Calendar.xml' > 'Calendar.xml'
		fi

		cp 'Calendar.xml' "Calendar-$(date +'%Y%m%d').xml" && \
		time java net.sf.saxon.Transform "${PWD}/Calendar.xml" "${PWD}/Calendar-OLM.xsl"

	elif [[ -r 'Calendar-OWA.txt' ]] ; then
		open 'https://outlook.office365.com/mail/'
		open 'Calendar-OWA.txt'
		echo 'Goto OWA, copy the HTML, and update Calendar-OWA.txt; press <Enter>.' > /dev/tty && read && \
		cp 'Calendar-OWA.txt' "Calendar-$(date +'%Y%m%d').txt" && \
		./owa.bash

	fi
	typeset returnCode=${?}
	typeset event_cnt=$(grep -c -e 'BEGIN:VEVENT' Calendar.ics)
	echo "Events found: ${event_cnt}" > /dev/tty

	# Flag an error if the calendar file is empty
	if (( ! returnCode && ! event_cnt )) ; then
		(( returnCode += 1 ))
	fi
	return ${returnCode}
}

echo 'Here We Go!!' \
	&& figlet 'OOOs and WFHs' \
	&& read -p'Goto Outlook and export your Calendar; press <Enter>.' 2>&1 \
	&& echo 'Creating ICS calendar extract ...' 2>&1 \
	&& transform > Calendar.ics \
	&& echo 'Uploading the Calendar ICS extract ...' \
	&& chmod -v 644 Calendar.ics \
	&& cp -v Calendar.ics Calendar-$(date +'%Y%m%d').ics \
	&& time curl --fail-with-body --user "${ICS_WEBDAV_ID}:${ICS_WEBDAV_PASSWORD}" --anyauth --upload-file 'Calendar.ics' "${ICS_WEBDAV_DEST%/}/Calendar.ics" \
	&& read -p'A new calendar file is read for your Android device to sync; press <Enter> to exit.' 2>&1 \
	&& exit 0

echo "ERROR! rc=${?}" 1>&2
read -p'An ERROR occurred; press <Enter> to exit.' 2>&1
exit 1
