<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:func="file://localhost/home/rodney/devl/Calendar-import/"
	extension-element-prefixes="xs func fn"
>

	<xsl:output method="text" />

	<xsl:param name="now" select="xs:dateTime(fn:current-date())" as="xs:dateTime" />
	<xsl:param name="now-week" select="$now + xs:dayTimeDuration('P7D')" as="xs:dateTime" />
	<xsl:param name="null-date" select="xs:dateTime('2001-01-01T00:00:00')" as="xs:dateTime" />


	<xsl:function name="func:cleanString">
		<xsl:param name="str" />
		<xsl:param name="repl" />
		<xsl:value-of select="substring((normalize-space(normalize-unicode($str)), $repl)[.][1], 1, 254)" />
	</xsl:function>

	<xsl:template match="appointments">
		<xsl:text>BEGIN:VCALENDAR
PRODID:-//localhost//Calendar.xsl
VERSION:2.0
METHOD:PUBLISH
BEGIN:VTIMEZONE
TZID:Central Standard Time
BEGIN:STANDARD
DTSTART:16011104T020000
RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:16010311T020000
RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
END:DAYLIGHT
END:VTIMEZONE

</xsl:text>

		<xsl:apply-templates select="appointment[
										'0' = OPFCalendarEventIsRecurring and
										(not(OPFCalendarEventCopyPrimaryCategory/OPFCategoryCopyName) or 'NoSpiraclock' != OPFCalendarEventCopyPrimaryCategory/OPFCategoryCopyName) and
										(OPFCalendarEventCopyStartTime &lt;= $now-week and OPFCalendarEventCopyEndTime >= $now)
										]">
			<xsl:sort select="OPFCalendarEventCopyStartTime" />
		</xsl:apply-templates>

		<xsl:text>END:VCALENDAR&#x0A;</xsl:text>
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopyFreeBusyStatus['0' = .]"><xsl:text>&#x0A;STATUS:Busy</xsl:text></xsl:template>
	<xsl:template match="OPFCalendarEventCopyFreeBusyStatus['1' = .]"><xsl:text>&#x0A;STATUS:Free</xsl:text></xsl:template>
	<xsl:template match="OPFCalendarEventCopyFreeBusyStatus['2' = .]"><xsl:text>&#x0A;STATUS:Tentative</xsl:text></xsl:template>
	<xsl:template match="OPFCalendarEventCopyFreeBusyStatus['3' = .]"><xsl:text>&#x0A;STATUS:OOO</xsl:text></xsl:template>
	<xsl:template match="OPFCalendarEventCopyFreeBusyStatus"><xsl:text>&#x0A;STATUS:UNKNOWN=</xsl:text><xsl:value-of select="." /></xsl:template>

	<xsl:template match="OPFCalendarEventCopyReminderDelta['0' = parent::*/OPFCalendarEventGetHasReminder]" />

	<xsl:template match="OPFCalendarEventCopyReminderDelta">
		<xsl:variable name="mins" select=". div 60" />
		<xsl:text>
BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:Reminder
TRIGGER:-PT</xsl:text><xsl:value-of select="$mins" /><xsl:text>M
END:VALARM</xsl:text>
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopyUUID">
		<xsl:text>&#x0A;UID:</xsl:text><xsl:value-of select="." />
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopyStartTime['0' != parent::*/OPFCalendarEventGetIsAllDayEvent]">
		<xsl:text>&#x0A;DTSTART;VALUE=DATE:</xsl:text><xsl:value-of select="format-dateTime(xs:dateTime(.), '[Y0001][M01][D01]')" />
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopyStartTime">
		<xsl:text>&#x0A;DTSTART:</xsl:text><xsl:value-of select="format-dateTime(xs:dateTime(.), '[Y0001][M01][D01]T[H01][m01][s01]Z')" />
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopyEndTime['0' != parent::*/OPFCalendarEventGetIsAllDayEvent]" />

	<xsl:template match="OPFCalendarEventCopyEndTime">
		<xsl:text>&#x0A;DTEND:</xsl:text><xsl:value-of select="format-dateTime(xs:dateTime(.), '[Y0001][M01][D01]T[H01][m01][s01]Z')" />
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopySummary">
		<xsl:text>&#x0A;SUMMARY:</xsl:text><xsl:value-of select="func:cleanString(., '(no subject)')" />
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopyDescriptionPlain">
		<xsl:text>&#x0A;DESCRIPTION:</xsl:text><xsl:value-of select="func:cleanString(., '(no description)')" />
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopyLocation">
		<xsl:text>&#x0A;LOCATION:</xsl:text><xsl:value-of select="func:cleanString(., '(no location)')" />
	</xsl:template>

	<xsl:template match="OPFCalendarEventCopyOrganizer">
		<xsl:text>&#x0A;ORGANIZER:mailto:</xsl:text><xsl:value-of select="." />
	</xsl:template>

	<xsl:template match="appointment">
		<xsl:text>BEGIN:VEVENT&#x0A;CLASS:PUBLIC</xsl:text>
		<xsl:apply-templates select="OPFCalendarEventCopyUUID,OPFCalendarEventCopyStartTime,OPFCalendarEventCopyEndTime,OPFCalendarEventCopySummary,OPFCalendarEventCopyDescriptionPlain,OPFCalendarEventCopyLocation,OPFCalendarEventCopyOrganizer,OPFCalendarEventCopyFreeBusyStatus,OPFCalendarEventCopyReminderDelta" />
		<xsl:text>&#x0A;END:VEVENT&#x0A;&#x0A;</xsl:text>

	</xsl:template>

<!--
	Maybe add attendee list to body, just for reference?
	OPFCalendarEventCopyAttendeeList (appointmentAttendee/(@OPFCalendarAttendeeAddress,@OPFCalendarAttendeeName)),

	Explanations of recurring patterns (TODO add recurring extracts in the future?)
	 2	<OPFRecurrencePatternType xml:space="preserve">OPFRecurrencePatternAbsoluteYearly</OPFRecurrencePatternType>
	 6	<OPFRecurrencePatternType xml:space="preserve">OPFRecurrencePatternDaily</OPFRecurrencePatternType>
	 2	<OPFRecurrencePatternType xml:space="preserve">OPFRecurrencePatternRelativeMonthly</OPFRecurrencePatternType>
	 1	<OPFRecurrencePatternType xml:space="preserve">OPFRecurrencePatternRelativeYearly</OPFRecurrencePatternType>
	36	<OPFRecurrencePatternType xml:space="preserve">OPFRecurrencePatternWeekly</OPFRecurrencePatternType>

	<xsl:template match="OPFRecurrencePatternType['OPFRecurrencePatternDaily' = .]">
		<xsl:copy-of select="." />
		<DEBUG_PT>FIXME:</DEBUG_PT>
	</xsl:template>

	<xsl:template match="OPFRecurrencePatternType['OPFRecurrencePatternWeekly' = .]">
		<xsl:copy-of select="." />
		<DEBUG_PT>On a day or days of each week of the month (or interval of weeks).</DEBUG_PT>
	</xsl:template>

	<xsl:template match="OPFRecurrencePatternType['OPFRecurrencePatternRelativeMonthly' = .]">
		<xsl:copy-of select="." />
		<DEBUG_PT>On a day or days of a specific week of the month.</DEBUG_PT>
	</xsl:template>

	<xsl:template match="OPFRecurrencePatternType['OPFRecurrencePatternAbsoluteYearly' = .]">
		<xsl:copy-of select="." />
		<DEBUG_PT>On a specific date each year.</DEBUG_PT>
	</xsl:template>

	<xsl:template match="OPFRecurrencePatternType['OPFRecurrencePatternRelativeYearly' = .]">
		<xsl:copy-of select="." />
		<DEBUG_PT>E.g. 'namecheap renewal' occurs on the 2nd Saturday of every January.</DEBUG_PT>
	</xsl:template>
-->

</xsl:stylesheet>
