<?xml version="1.0"?>
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:saxon="http://saxon.sf.net/"
	xmlns:sql="java:/net.sf.saxon.sql.SQLElementFactory"
	xmlns:func="file://localhost/home/rodney/devl/Calendar-import/"
	extension-element-prefixes="xs saxon sql func fn">

<xsl:output method="text" />

<xsl:param name="base-dir" select="'c:/Users/e43921.LUV/LocalProgramFiles/cygwin-home/e43921/devl/Calendar-import'" />


<!-- insert your database details here, or supply them in parameters -->
<xsl:param name="driver" select="'org.xbib.jdbc.csv.CsvDriver'" />
<xsl:param name="database" select="concat('jdbc:xbib:csv:', $base-dir)" />
<xsl:param name="user" />
<xsl:param name="password" />

<xsl:variable name="special-chars"> *!:;,\/()[]"'&#x0A;&#x0D;</xsl:variable>

<xsl:variable name="show-time-as-cde">
	<cdes>
		<cde val="0">Working Elsewhere</cde> <!-- new in Win10 -->
		<cde val="1">Tentative</cde>
		<cde val="2">Busy</cde>
		<cde val="3">Free</cde>
		<cde val="4">OOO</cde>
		<cde val="na">** UNKNOWN : </cde>
	</cdes>
</xsl:variable>


<xsl:template name="main">
	<xsl:variable name="now" select="xs:dateTime(fn:current-date())" as="xs:dateTime" />
	<xsl:variable name="now-week" select="$now + xs:dayTimeDuration('P7D')" as="xs:dateTime" />

	<xsl:if test="not(element-available('sql:connect'))">
		<xsl:message>sql:connect is not available</xsl:message>
	</xsl:if>

	<xsl:variable name="conn" as="java:java.lang.Object" xmlns:java="http://saxon.sf.net/java-type">
		<sql:connect driver="{$driver}" database="{$database}" user="{$user}" password="{$password}">
			<xsl:fallback>
				<xsl:message terminate="yes">Failed to connect to <xsl:value-of select="$database" />.  SQL extensions are not installed.</xsl:message>
			</xsl:fallback>
		</sql:connect>
	</xsl:variable>


	<!--
	Result Set Order (please update if/when you change the SELECT statement below and fix tranformer [template match Event] that is Prop order dependent):
		1:		Subject
		2:		Description
		3:		Required Attendees
		4:		Optional Attendees
		5:		Categories
		6:		Show time as
		7:		Location
		8:		All day event
		9:		Reminder on/off
		10:	Meeting Organizer
		11:	Start Date
		12:	Start Time
		13:	End Date
		14:	End Time
		15:	Reminder Date
		16:	Reminder Time
	-->

	<xsl:variable name="cal-events">
		<sql:query
			connection="$conn"
			table="Calendar"
			column="
Subject,
Description,
&quot;Required Attendees&quot;,
&quot;Optional Attendees&quot;,
Categories,
&quot;Show time as&quot;,
Location,
&quot;All day event&quot;,
&quot;Reminder on/off&quot;,
&quot;Meeting Organizer&quot;,
&quot;Start Date&quot;,
&quot;Start Time&quot;,
&quot;End Date&quot;,
&quot;End Time&quot;,
&quot;Reminder Date&quot;,
&quot;Reminder Time&quot;
"

			where="1=1
AND Subject NOT LIKE 'Canceled: %'
AND Categories NOT LIKE '%NoSpiraclock%'
"

			row-tag="Event" column-tag="Prop"
		/>
	</xsl:variable>

<!--
AND Subject NOT LIKE 'Canceled: %'
AND	(Categories IS NULL OR Categories NOT LIKE '%NoSpiraclock%')
AND	TO_DATE(StartDate, 'MM/DD/YYYY') &gt;= TRUNC(CURRENT_DATE)
AND	(
			TO_DATE(StartDate, 'MM/DD/YYYY') &lt; DATE_ADD(TRUNC(CURRENT_DATE), INTERVAL 7 DAY)

		/* To get *my* future all day events */
		OR	(
			'Rodney Stromlund' = MeetingOrganizer
			AND 'TRUE' = Alldayevent
			AND RequiredAttendees IS NULL
			AND OptionalAttendees IS NULL
			)
		)
-->


	<!-- ========== Transform Event/Prop result set into something more XML intelligible ========== -->
	<xsl:variable name="xml-events">
		<xsl:apply-templates select="$cal-events/Event" />
	</xsl:variable>


	<!-- ******************** Begin output of ICS calendar text ******************** -->
	<xsl:text>BEGIN:VCALENDAR
PRODID:-//localhost//Calendar.xsl
VERSION:2.0
METHOD:PUBLISH
BEGIN:VTIMEZONE
TZID:Central Standard Time
BEGIN:STANDARD
DTSTART:16011104T020000
RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:16010311T020000
RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
END:DAYLIGHT
END:VTIMEZONE
</xsl:text>

	<xsl:apply-templates select="$xml-events/appt[xs:dateTime(start) ge $now and xs:dateTime(start) lt $now-week]">
		<xsl:sort select="start" />
	</xsl:apply-templates>

	<xsl:text>
END:VCALENDAR
</xsl:text>
	<!-- ******************** End output of ICS calendar text ******************** -->

	<sql:close connection="$conn" />
</xsl:template>


<xsl:function name="func:SQL-to-dateTime" as="xs:dateTime">
	<xsl:param name="dt" />	<!-- e.g. 1/4/2019 -->
	<xsl:param name="tm" />	<!-- e.g. 1:45:00 PM -->

	<xsl:variable name="date-list"	select="fn:tokenize($dt, '/')" />
	<xsl:variable name="time-list"	select="fn:tokenize($tm, ':')" />

	<xsl:variable name="year"			select="$date-list[3]" />
	<xsl:variable name="month"			select="format-number(xs:integer($date-list[1]), '00')" />
	<xsl:variable name="day"			select="format-number(xs:integer($date-list[2]), '00')" />

	<xsl:variable name="am-pm"			select="substring($time-list[3], 4, 2)" />
	<xsl:variable name="hour"			select="format-number((if ('PM' = $am-pm) then 12 else 0) + (if ('12' = $time-list[1]) then 0 else xs:integer($time-list[1])), '00')" />
	<xsl:variable name="minute"		select="format-number(xs:integer($time-list[2]), '00')" />
	<xsl:variable name="second"		select="format-number(xs:integer(substring($time-list[3], 1, 2)), '00')" />

	<xsl:variable name="xs-dt"			select="concat($year, '-', $month, '-', $day, 'T', $hour, ':', $minute, ':', $second)" />

	<!-- WARNING!: This sets the timezone to the timezone *NOW*. It does not make it apropos to the date, XSLT2 does not have -->
	<!-- that built in.  Create an XML database or something for lookup if you need the timezone calculated for past/future events. -->

	<!-- INFO: I did not do that; for now just do not run extracts across Sundays and you should be fine. Good enough for adhoc. -->
	<!-- At worst things will be 1 hour off and you will be notified and hour early or late. :/ -->

	<xsl:value-of select="adjust-dateTime-to-timezone(xs:dateTime($xs-dt))" />
</xsl:function>


<!--
	Reformat SQL result set as such:
		<appt @reminder="{xs:date-time}" @all-day-event="1" @show-time-as="Tentative,Busy,etc" @categories="NoSpiralClock" @location="Cube">
			<subject />
			<description />
			<required-attendees />
			<optional-attendees />
			<meeting-organizer />
			<start />
			<end />
		</appt>

	Make sure we have all our SQL fields b/f processing.
-->
<xsl:template match="Event[Prop[16]]">
	<appt>
		<xsl:attribute name="show-time-as" select="($show-time-as-cde/cdes/cde[@val = current()/Prop[6]], concat($show-time-as-cde/cdes/cde['na' = @val], current()/Prop[6]))[1]" />

		<xsl:if test="'False' != Prop[8] and '0' != Prop[8]">
			<xsl:attribute name="all-day-event" select="1" />
		</xsl:if>

		<xsl:if test="'False' != Prop[9] and '0' != Prop[9]">
			<xsl:attribute name="reminder" select="func:SQL-to-dateTime(Prop[15], Prop[16])" />
		</xsl:if>

		<xsl:if test="'' != Prop[5]">
			<xsl:attribute name="categories" select="Prop[5]" />
		</xsl:if>

		<xsl:if test="'' != Prop[7]">
			<xsl:attribute name="location" select="Prop[7]" />
		</xsl:if>

		<start><xsl:value-of select="func:SQL-to-dateTime(Prop[11], Prop[12])" /></start>
		<end><xsl:value-of select="func:SQL-to-dateTime(Prop[13], Prop[14])" /></end>

		<subject><xsl:value-of select="normalize-space(Prop[1])" /></subject>
		<description><xsl:value-of select="normalize-space(Prop[2])" /></description>

		<meeting-organizer><xsl:value-of select="Prop[10]" /></meeting-organizer>
		<required-attendees><xsl:value-of select="Prop[3]" /></required-attendees>
		<xsl:if test="'' != Prop[4]">
			<optional-attendees><xsl:value-of select="Prop[4]" /></optional-attendees>
		</xsl:if>
	</appt>
</xsl:template>


<xsl:function name="func:cleanString">
	<xsl:param name="str" />
	<xsl:param name="repl" />

	<xsl:value-of select="substring((normalize-space(normalize-unicode($str)), $repl)[.][1], 1, 254)" />
</xsl:function>


<xsl:function name="func:formatEmail">
	<xsl:param name="str" />
	<!--
		E.g. "Rodney Stromlund" == "Rodney.Stromlund@wnco.com"
		E.g. "Javed Syed (Contractor)" == "Javed.Syed@wnco.com"
		TODO: fixup DG-es and more emails with names that do not match address (e.g. David.Hernandez3@wnco.com)
	-->
	<xsl:variable name="name" select="translate(translate(if (contains($str, ' &#x28;')) then substring-before($str, ' &#x28;') else $str, ' ', '.'), $special-chars, '')" />
	<xsl:variable name="email" select="concat($name, if (contains($str, '@')) then '' else '@wnco.com')" />

	<xsl:choose>
		<xsl:when test="'David.Hernandez@wnco.com' = $email">
			<xsl:text>David.Hernandez3@wnco.com</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$email" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>


<xsl:function name="func:showDate">
	<xsl:param name="isc-prefix" /><!--DTSTART, DTEND-->
	<xsl:param name="dt-tm" as="xs:dateTime" /><!--yyyy-mm-dd hh:mm:ss-->
	<xsl:param name="is-all-day" /><!--0 or non-0-->

	<xsl:variable name="dt_Zulu" select="adjust-dateTime-to-timezone($dt-tm, xs:dayTimeDuration('-PT0H'))" as="xs:dateTime" />

	<xsl:choose>
		<xsl:when test="not($is-all-day)">
			<xsl:value-of select="concat($isc-prefix, ':', format-dateTime($dt_Zulu, '[Y0001][M01][D01]T[H01][m01][s01]Z'))" />
		</xsl:when>
		<xsl:when test="$is-all-day and ('DTSTART' = $isc-prefix)">
			<xsl:value-of select="concat($isc-prefix, ';VALUE=DATE:', format-dateTime($dt_Zulu, '[Y0001][M01][D01]'))" />
		</xsl:when>
		<xsl:otherwise>
			<!-- Here is DTEND and is-all-day ... output nothing -->
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>


<xsl:template match="appt" priority="0.2">
	<xsl:text>
BEGIN:VEVENT
CLASS:PUBLIC
UID:content://localhost/events/?start=</xsl:text><xsl:value-of select="translate(start, $special-chars, '')" /><xsl:text>&amp;subject=</xsl:text>
	<xsl:value-of select="translate(func:cleanString(subject, 'no subject'), $special-chars, '+')" />
	<xsl:text>&#x0A;</xsl:text>

<xsl:value-of select="func:showDate('DTSTART', start, abs(@all-day-event))" /><xsl:text>&#x0A;</xsl:text>

<xsl:value-of select="func:showDate('DTEND', end, abs(@all-day-event))" />

<xsl:text>&#x0A;SUMMARY:</xsl:text><xsl:value-of select="func:cleanString(subject, '(no subject)')" />

<xsl:text>&#x0A;DESCRIPTION:</xsl:text>
	<xsl:value-of select="func:cleanString(description, '(no description)')" />
	<xsl:text> -- Required Attendees: </xsl:text>
	<xsl:value-of select="func:cleanString(required-attendees, '')" />
	<xsl:text> -- Optional Attendees: </xsl:text>
	<xsl:value-of select="func:cleanString(optional-attendees, '')" />
	<xsl:text> -- Category: </xsl:text>
	<xsl:value-of select="func:cleanString(@categories, '')" />
	<xsl:text> -- </xsl:text>
	<xsl:value-of select="func:cleanString(@show-time-as, '')" />

<xsl:text>&#x0A;LOCATION:</xsl:text><xsl:value-of select="func:cleanString(@location, '(no location)')" />

<xsl:text>&#x0A;ORGANIZER:mailto:</xsl:text><xsl:value-of select="func:formatEmail(meeting-organizer)" />

<!--
Android Calendar sync doesnt seem to import these, dont waste the space.

<xsl:for-each select="fn:tokenize(required-attendees, ';')">
	<xsl:text>&#x0A;ATTENDEE;ROLE=REQ-PARTICIPANT:mailto:</xsl:text>
	<xsl:value-of select="func:formatEmail(.)" />
</xsl:for-each>

<xsl:for-each select="fn:tokenize(optional-attendees, ';')">
	<xsl:text>&#x0A;ATTENDEE;ROLE=OPT-PARTICIPANT:mailto:</xsl:text>
	<xsl:value-of select="func:formatEmail(.)" />
</xsl:for-each>

-->

<!--
	selfAttendeeStatus:
		0 = (No Response)
		1 = Yes
		2 = No
		4 = Maybe

	Map:
		Maybe(4) = Tentative
		No(2) = Free
		Yes(1) = everything else (Busy, OOO, etc...)
-->
<xsl:text>&#x0A;STATUS:</xsl:text><xsl:value-of select="if ('Free' = @show-time-as or 'Tentative' = @show-time-as) then 'TENTATIVE' else 'CONFIRMED'" />

<xsl:text>&#x0A;</xsl:text>


	<!-- This is for 'hasAlarm', we need to output a Reminder node. -->
	<!-- Delegate outputting the element to another rule match. -->
	<xsl:next-match />

	<xsl:text>END:VEVENT&#x0A;</xsl:text>
</xsl:template>


<!-- No "VALARM" for you ... -->
<xsl:template match="appt[not(@reminder)]" priority="0.1" />


<!-- Yes "VALARM" for you ... -->
<xsl:template match="appt[@reminder]" priority="0.1">
	<xsl:text>BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:Reminder
TRIGGER:</xsl:text><xsl:value-of select="xs:dateTime(@reminder) - xs:dateTime(start)" /><xsl:text>
END:VALARM
</xsl:text>

</xsl:template>


</xsl:stylesheet>
