<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:func="file://localhost/home/rodney/miscweb/automated/comics/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	exclude-result-prefixes="xhtml xs func"
>

<xsl:output method="text" />

<xsl:param name="current_year"   select="-2013" />
<xsl:param name="current_month"  select="-1" />
<xsl:param name="current_day"    select="-5" />
<xsl:param name="current_dow"    select="'???'" />

<xsl:template match="text()" />

<xsl:template match="*['Repeating event that was moved' = @aria-label]" priority="4">
	<xsl:apply-templates />
</xsl:template>

<xsl:template match="xhtml:div[(contains(@aria-label, 'event') or contains(@title, 'eeting')) and @aria-label != 'New event' and not(contains(@id, 'selectedInterval'))]">
	<xsl:text>id(</xsl:text>
	<xsl:value-of select="ancestor::xhtml:div[@data-calitemid]/@data-calitemid" />
	<xsl:text>) </xsl:text>

	<xsl:value-of select="@aria-label" />
	<xsl:text>&#x0A;~&#x0A;</xsl:text>
	<xsl:apply-templates />
</xsl:template>

</xsl:stylesheet>
