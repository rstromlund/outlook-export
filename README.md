# Outlook Export

Massage Outlook's CSV export into an Android compatible ICS for import. To use:
   * Export Outlook calendar file to "Calendar.csv"
	* Run "upload.bash" to create an ICS file and upload it to a WebDAV server

Required:
   * Saxon XSLT processor (net.sf.saxon.Transform)

Installation/Setup:
   * Copy DOT.appauth to ~/.appauth
   * chmod 600 ~/.appauth
   * Edit ~/.appauth and modify values for your WEBDAV server

DavMail

$ curl -u e43921:Taush222 -vvv http://127.0.0.1:1080/users/rodney.stromlund@wnco.com/calendar/


